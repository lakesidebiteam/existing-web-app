import pypyodbc


class GenQuery:
    def __init__(self, connection, user, columns, tables, order_by=None, limit=None):
        self.connection = connection
        self.columns = columns
        self.user = user
        self.tables = tables
        self.where_list = []
        self.multivalue_list = []
        self.order = order_by if order_by else ""

        if not limit:
            limit = 10
        try:
            self.limit = int(limit)
        except:
            self.limit = 10

        if limit <= 0:
            self.limit = 10
        if limit > 1000:
            self.limit = 1000

    def create_where_item(self, variable_name, variable_type, where_text, value):

        self.where_list.append(
            {
                "key": variable_name,
                "type": variable_type,
                "text": where_text,
                "value": value,
            }
        )

    def create_multivalue_item(self, variable_name, item_list, alias, on):
        """
        

        Parameters
        ----------
        variable_name : str
            Name of variable
        item_list : str or list
            List of values or string of comma separated values
        alias : str
            sql alias to use for table function
        on : str
            join conditions for table function

        Returns
        -------
        None.

        """

        if type(item_list) == list:
            item_list = ",".join([str(item) for item in item_list])

        self.multivalue_list.append(
            {"key": variable_name, "text": item_list, "alias": alias, "on": on}
        )

    def _generate_where(self):

        where_list = []
        variable_list = []

        self.variable_text = ""

        self.values = []

        for filter in self.where_list:

            variable_list.append((filter["key"], filter["type"]))

            self.values.append(filter["value"])

            where_list.append(f" ({filter['text']}) ")

        if len(self.where_list) == 0:
            self.where_text = "1 = 1"
        else:
            self.where_text = " and ".join(where_list)

        for variable in variable_list:

            self.variable_text += f" Declare @{variable[0]} {variable[1]} = ? "

    def _generate_multivalue(self):

        mv_list = []
        variable_list = []

        self.multivalue_text = ""

        for mv in self.multivalue_list:

            variable_list.append(mv["key"])

            self.values.append(mv["text"])

            self.multivalue_text += f" JOIN dbo.udfSplitString(@{mv['key']}, ',') as {mv['alias']}  ON {mv['on']} "

        for variable in variable_list:

            self.variable_text += f" Declare @{variable} varchar(max) = ? "

    def _generate_query(self):

        top = f"TOP {self.limit}"

        columns_text = ", ".join(self.columns)

        self._generate_where()
        self._generate_multivalue()

        if self.order:
            self.order = f"ORDER BY {self.order}"
        else:
            self.order = ""

        self.sql_command = f"EXECUTE AS USER = ? {self.variable_text} SELECT {top} {columns_text} FROM {self.tables} {self.multivalue_text} WHERE {self.where_text} {self.order} REVERT"

        self.return_variables = [self.user]

        self.return_variables.extend(self.values)

    def execute_query(self):

        self._generate_query()

        cursor = self.connection.cursor()

        cursor.execute(self.sql_command, self.return_variables)

        self.data = cursor.fetchall()

        cursor.commit()

        self.connection.close()

        return self.data

