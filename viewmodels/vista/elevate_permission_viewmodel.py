from operator import itemgetter

from flask.globals import request
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.create_filter import create_filter
from services.vista import Vista
from flask import Markup, jsonify, flash
from lakeside.connection.sql_server import LakesideConnections
from lakeside.tools.email import Email
from lakeside.log import create_log


class ElevateVistaPermissions(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "Elevate Permissions"
        self.page_header = "Elevate Permissions"

        self.lc = LakesideConnections()

        self.__get_admin_options()

        

    def __get_admin_options(self):
        con = self.lc.viewpoint()
        cursor = con.cursor()

        command = """SELECT   name
                    FROM     master.sys.server_principals 
                    WHERE    UPPER(name) like 'LAKESIDEIND\ADMIN%' AND UPPER(name) not like '%ADMINAP' and UPPER(name) not like '%ADMINKG'
                    ORDER BY name"""

        cursor.execute(command)

        data = cursor.fetchall()

        cursor.close()

        self.admin_options = [row[0] for row in data]

    def process_results(self):

        log_id = create_log(self.lc.source_data(), task="Elevate Vista Permissions")

        try:

            results = self.request.form

            account = results.get('account')
            time = results.get('time')
            reason = results.get('reason')

            requested_by = self.email_login

            con = self.lc.create_sql_connection(server="ERPSQLSRV", database="Tasks")
            cursor = con.cursor()

            time_int = min(int(time), 120)

            cursor.execute('{ CALL pGrantTemporaryAdmin(?, ?) }', [account, time_int])
            cursor.commit()
            cursor.close()
        
            self.__send_alert(account, time, reason, requested_by)

            flash(f"Admin access granted for {results['account']}", 'table-success')
            create_log(self.lc.source_data(), id=log_id, status='success', message=f'Admin access granted to {account} for {time} minutes. Requested by: {requested_by}. Reason: {reason}')

        except Exception as e:
            print(str(e))
            create_log(self.lc.source_data(), id=log_id, status='failure', message=str(e))

    def __send_alert(self, account, time, reason, requested_by):
        em = Email("Admin Access Request on ERPSQLSRV")

        em.add_recipient('Aaron Predmore', 'aaron.predmore', 'lakesideindustries.com')

        em.plain_message = f'Admin access requested by {requested_by} for {account} for {time} minutes.\n\n'
        em.plain_message += f'Reason:\n{reason}'

        em.html_message = """Admin access requested on ERPSQLSRV<br><br>
                            <table>
                                <tr>
                                    <td>
                                        <strong>User:</strong>
                                    </td>
                                    <td>{requested_by}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Account:</strong>
                                    </td>
                                    <td>{account}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Minutes:</strong>
                                    </td>
                                    <td>{time}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Reason:</strong>
                                    </td>
                                    <td>{reason}</td>
                                </tr>
                            </table>"""

        em.html_message = em.html_message.format(requested_by = requested_by, account=account, time=time, reason=reason)


        em.process_email()

    


