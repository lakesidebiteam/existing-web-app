from flask.globals import request
from flask import flash, Markup
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.connections import dw, automated_tasks
from collections import OrderedDict


class AsphaltProjectionsViewmodel(ViewModelBase):
    def __init__(self) -> None:
        super().__init__()

        self.page = "Asphalt Projections"
        self.page_header = "Asphalt Projections"

        self.month = self.request_dict.get("month", None)
        self.division = self.request_dict.get("division", None)
        self.edit = self.request_dict.get("edit", None) == "y"

    def get_month(self):
        connection = automated_tasks()
        cursor = connection.cursor()

        if self.month is None:
            command = """Select 
                            DATEADD(month, -1, DATEADD(day, 1, EOMONTH(GETDATE(), -1))), 
                            DATEADD(day, 1, EOMONTH(GETDATE(), -1)), 
                            DATEADD(month, 1, DATEADD(day, 1, EOMONTH(GETDATE(), -1))), 
                            MONTH(GETDATE()),
                            YEAR(DATEADD(month, 2, GETDATE()))"""
            cursor.execute(command)
        else:
            command = """DECLARE @Month date
                     SET @Month = ISNULL(TRY_CAST(? as date), DATEADD(day, 1, EOMONTH(GETDATE(), -1)))

                     Select 
                        DATEADD(month, -1, @Month), 
                        @Month, 
                        DATEADD(month, 1, @Month),
                        MONTH(@Month),
                        YEAR(DATEADD(month, 2, @Month))"""

            cursor.execute(command, [self.month])

        data = cursor.fetchone()

        self.month = data[1]

        self.prior_month = data[0]
        self.next_month = data[2]
        self.month_number = data[3]
        self.fiscal_year = data[4]

    def get_divisions(self):
        connection = automated_tasks()
        cursor = connection.cursor()

        cursor.execute("{CALL WebApp.pAsphaltProjectionsDivisions(?)}", [self.month])

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        div_dict = OrderedDict()

        for row in data:
            div_dict[row[0]] = {
                "description": row[1],
                "status": row[2],
            }
        self.divisions = div_dict

        if (
            self.division is None
            or self.division == ""
            or self.division not in self.divisions.keys()
        ):
            self.division = [i for i in self.divisions.keys()][0]

        if self.divisions[self.division]["status"] == "Open":
            self.edit = True

    def get_projections(self):
        connection = automated_tasks()
        cursor = connection.cursor()

        cursor.execute(
            "{CALL WebApp.pGetAsphaltProjections(?, ?)}", [self.month, self.division]
        )

        data = cursor.fetchall()

        connection.commit()
        connection.close()

        proj_dict = OrderedDict()

        self.months = []
        self.month_header = []
        self.plants = []

        self.mth_abbr = []

        for row in data:

            mth = row[0]
            status = row[1]
            type_id = row[2]
            plant = row[3] if row[5] is None else row[5]
            projection = "" if row[6] is None else row[6]
            proj_id = row[7]
            mth_hdr = row[8]

            # Add Month
            if mth not in self.months:
                self.months.append(mth)
                self.month_header.append(mth_hdr)
                proj_dict[mth] = OrderedDict()
                if len(self.mth_abbr) < 12:
                    self.mth_abbr.append(mth_hdr[:3])

            # Add Type
            if type_id not in proj_dict[mth].keys():
                proj_dict[mth][type_id] = OrderedDict()

            if plant not in self.plants and row[5] is not None:
                self.plants.append(plant)

            # add comma and star
            proj_txt = '*' if status == 'Closed' and projection != '' else ''
            if projection != '':
                proj_txt += "{:,}".format(int(projection)) if (status == 'Closed' or not self.edit) and status != '' else str(projection)

            proj_dict[mth][type_id][plant] = {
                "status": status,
                "projection": proj_txt,
                "proj_id": proj_id,
                "mth_hdr": mth_hdr,
            }

        self.projections = proj_dict

        self.classes = ["contract", "future"]
        self.classes.extend(["plant" + i for i in self.plants])

        self.cols = ["job" + i for i in self.month_header]
        self.cols.extend(["plant" + i for i in self.month_header])
        self.cols.append("jobTotal")
        self.cols.append("plantTotal")

        self.cols = Markup(self.cols)
        self.classes = Markup(self.classes)

        self.get_job_history()
        self.get_plant_history()

    def get_job_history(self):
        connection = automated_tasks()
        cursor = connection.cursor()

        cursor.execute(
            "{CALL WebApp.pGetHistoricJobTons(?, ?)}", [self.month, self.division]
        )

        data = cursor.fetchall()

        self.job_history = OrderedDict()
        self.job_history_total = OrderedDict()

        for row in data:
            if row[0] not in self.job_history.keys():
                self.job_history[row[0]] = []
                self.job_history_total[row[0]] = 0

            self.job_history[row[0]].append(
                 ("*" if row[3] == "Current" else "") + "{:,}".format(row[4])
            )
            self.job_history_total[row[0]] += row[4]

        for i, total in self.job_history_total.items():
            self.job_history_total[i] = "{:,}".format(total)

    def get_plant_history(self):
        connection = automated_tasks()
        cursor = connection.cursor()

        cursor.execute(
            "{CALL WebApp.pGetHistoricPlantTons(?, ?)}", [self.month, self.division]
        )

        data = cursor.fetchall()

        self.plant_history = OrderedDict()
        self.plant_history_total = OrderedDict()

        for row in data:

            if row[0] not in self.plant_history.keys():
                self.plant_history[row[0]] = OrderedDict()
                self.plant_history_total[row[0]] = OrderedDict()

            if row[1] not in self.plant_history[row[0]].keys():
                self.plant_history[row[0]][row[1]] = []
                self.plant_history_total[row[0]][row[1]] = 0

            self.plant_history[row[0]][row[1]].append(
                 ("*" if row[4] == "Current" else "") + "{:,}".format(row[5])
            )
            self.plant_history_total[row[0]][row[1]] += row[5]

        for key in self.plant_history_total.keys():
            for i, total in self.plant_history_total[key].items():
                self.plant_history_total[key][i] = "{:,}".format(total)

    def process_results(self):

        try:
            connection = automated_tasks()

            for key, value in request.form.items():

                keys = key.split("|")

                if len(keys) == 5:
                    div, mth, proj_mth, type_id, plant = keys
                else:
                    continue
                cursor = connection.cursor()

                cursor.execute(
                    "{ CALL WebApp.pAddEditAsphaltProjection(?, ?, ?, ?, ?, ?, ?)}",
                    [
                        div,
                        mth,
                        proj_mth,
                        type_id,
                        None if plant == "" else plant,
                        value.replace(",", ""),
                        self.email_login,
                    ],
                )
            
            connection.commit()
            connection.close()

            flash("Results Saved Successfully", "table-success")

        except Exception as e:
            print(str(e))
            flash("Error Saving Results", "table-danger")

