from views.home_views import blueprint
from flask import Blueprint, request, redirect, flash

from lakeside.tasks.active_directory import ActiveDirectory
from lakeside.connection.sql_server import LakesideConnections

from viewmodels.shared.viewmodelbase import ViewModelBase
from viewmodels.backlog_viewmodel import BacklogViewmodel
from viewmodels.data_warehouse.reload_tables_viewmodel import ReloadTableViewModel
from viewmodels.data_warehouse.run_path_viewmodel import RunPathViewModel
from viewmodels.data_warehouse.run_stream_viewmodel import RunStreamViewModel
from viewmodels.data_warehouse.common_tasks_viewmodel import RunCommonTasks
from viewmodels.data_warehouse.dw_users_viewmodel import DataWarehouseUsers
from viewmodels.data_warehouse.security_templates_viewmodel import SecurityTemplates
from viewmodels.monroe_paving_viewmodel import MonroePavingViewmodel
from infrastructure.view_modifiers import response
from services.active_directory import ldap
from services.data_warehouse import DataWarehouse


blueprint = Blueprint("data_warehouse", __name__, template_folder="templates")


@blueprint.route("/dw/reloadtable", methods=["GET", "POST"])
@response(template_file="data_warehouse/reloadtable.html")
@ldap.group_required(["APP_BIUser"])
def reload_table():
    vm = ReloadTableViewModel()

    print(vm.action)

    if vm.action == "reload":
        return redirect(vm.url)

    if vm.action == "reload_selected":
        vm.dw.reload_table_task(vm.table_dict)
        return redirect(vm.url)

    result = vm.to_dict()
    result["filters"] = vm.create_filters()
    result["table"] = vm.build_table()
    result["submit_buttons"] = vm.get_filter_buttons()

    if vm.action == "reload_table":
        vm.dw.reload_table(
            vm.reload_table_id, vm.reload_table_source, vm.reload_table_job
        )
    elif vm.action == "recreate_hash":
        vm.dw.recreate_hash(vm.reload_table_id)

    return result


# RunTask
@blueprint.route("/dw/runtask", methods=["GET", "POST"])
@response(template_file="shared/_buttoncards.html")
@ldap.group_required(["APP_BIUser"])
def run_task():
    vm = RunCommonTasks()
    result = vm.to_dict()

    if vm.action == "run_task":
        if vm.action_id == "1":
            try:
                vm.dw.deploy_security()
                flash("Security settings successfully deployed.", "table-success")
            except Exception as e:
                flash(f"Error deploying security: {str(e)}", "table-danger")
        elif vm.action_id == "2":
            try:
                ad = ActiveDirectory()
                ad.reload_person()
                flash("ActiveDirectory.Person Completed", "table-success")
            except Exception as e:
                flash(
                    f"Error deploying reloading ActiveDirectory.Person: {str(e)}",
                    "table-danger",
                )

    return result


# Reload Paths
@blueprint.route("/dw/reloadpath", methods=["GET", "POST"])
@response(template_file="shared/_buttoncards.html")
@ldap.group_required(["APP_BIUser", "APP_RunPath"])
def reload_path():
    vm = RunPathViewModel()
    result = vm.to_dict()

    print(vm.action)

    if vm.action == "run_path":
        vm.dw.run_path(vm.path_id)

    return result


# Relaod Streams
@blueprint.route("/dw/reloadstream", methods=["GET", "POST"])
@response(template_file="shared/_buttoncards.html")
@ldap.group_required(["APP_BIUser"])
def reload_stream():
    vm = RunStreamViewModel()
    result = vm.to_dict()

    print(vm.action)

    if vm.action == "run_stream":
        vm.dw.run_stream(vm.stream_id)

    return result


@blueprint.route("/dw/api/<resource>", methods=["GET", "POST"])
def get_resource(resource):

    schema_id = request.args.get("schemaid", type=int, default=None)
    database_id = request.args.get("dbid", type=int, default=None)

    dw = DataWarehouse()

    if resource.lower() == "getschemas":
        dw.get_schemas(database_id)
        return dw.schemas
    elif resource.lower() == "getdatabases":
        dw.get_databases()
        return dw.databases
    elif resource.lower() == "gettables":
        dw.get_tables(database_id, schema_id)
        return dw.tables


# Data Warehouse Users
@blueprint.route("/dw/users", methods=["GET"])
@response(template_file="shared/_filtertable.html")
@ldap.group_required(["APP_BIUser"])
def users_get():
    vm = DataWarehouseUsers()
    result = vm.to_dict()
    result["table"] = vm.build_table()
    result["modal_items"] = vm.build_new_user_form()

    return result


@blueprint.route("/dw/users", methods=["POST"])
@ldap.group_required(["APP_BIUser"])
def users_post():
    vm = DataWarehouseUsers()
    vm.process_results()
    return redirect("/dw/users")


# Data Warehouse Security Templates
@blueprint.route("/dw/templates/<template_id>", methods=["GET"])
@blueprint.route("/dw/templates/", methods=["GET"])
@response(template_file="data_warehouse/security_templates.html")
@ldap.group_required(["APP_BIUser"])
def templates_get(template_id=None):

    try:
        template_id = int(template_id)
    except:
        template_id = None

    vm = SecurityTemplates(template_id)

    if template_id is None:
        return redirect(f"/dw/templates/{vm.get_default_template()}")

    vm.get_info()

    result = vm.to_dict()

    return result


@blueprint.route("/dw/templates/<template_id>", methods=["POST"])
@blueprint.route("/dw/templates", methods=["POST"])
@ldap.group_required(["APP_BIUser"])
def templates_post(template_id=None):

    try:
        template_id = int(template_id)
    except:
        template_id = None

    vm = SecurityTemplates(template_id)

    vm.process_results()

    return redirect(f"/dw/templates/{vm.selected_template_id}")


@blueprint.route("/backlog/<filter>", methods=["GET"])
@blueprint.route("/backlog", methods=["GET"])
@response(template_file="backlog.html")
@ldap.group_required(["APP_BIUser"])
def backlog_get(filter="Open"):

    vm = BacklogViewmodel(filter)
    vm.get_combo_data()
    vm_dict = vm.to_dict()

    return vm_dict


@blueprint.route("/backlog/<filter>", methods=["POST"])
@blueprint.route("/backlog", methods=["POST"])
@ldap.group_required(["APP_BIUser"])
def backlog_post(filter="Open"):

    vm = BacklogViewmodel()
    if "filterSelect" in request.form.keys():
        return redirect(f"/backlog/{request.form['filterSelect']}")
    vm.process_results()

    return redirect(f"/backlog/{filter}")


@blueprint.route("/backlog/items/<item_id>", methods=["GET"])
@ldap.group_required(["APP_BIUser"])
def backlog_item_get(item_id):

    vm = BacklogViewmodel()
    vm.get_item_details(item_id)
    return vm.backlog_item


@blueprint.route("/monroe-paving-calendar", methods=["GET"])
@blueprint.route("/monroe-paving-calendar/<item_id>", methods=["GET"])
@response(template_file="monroe_paving.html")
@ldap.login_required
def monroe_paving_get(item_id=None):
    
    vm = MonroePavingViewmodel(item_id)

    return vm.process()

