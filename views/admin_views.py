from flask import Blueprint, g, request, session, redirect, url_for, flash
from services.active_directory import ldap
from infrastructure.view_modifiers import response
from viewmodels.shared.viewmodelbase import ViewModelBase

blueprint = Blueprint("admin", __name__, template_folder="templates")



@blueprint.route("/admin/dashboard", methods=["GET", "POST"])
@response(template_file="admin/dashboard.html")
@ldap.group_required(["APP_Admin"])
def admin_dashboard():
    vm = ViewModelBase()

    vm.page="Admin Dashboard"

    return vm.to_dict()
