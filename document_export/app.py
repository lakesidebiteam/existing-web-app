import os
from flask import Flask, g, session
from services.active_directory import ldap
from services.ad_functions import get_app_groups
from flask_debugtoolbar import DebugToolbarExtension


app = Flask(__name__)

SECRET_KEY = os.urandom(32)
app.config[
    "SECRET_KEY"
] = b"\xf4\x19 \xe5Q8\xd5b\x95,\x8f\xd0_\x83*\xd8[\xe0\xd9?\x80\xeb\x1bO0\x891\xc7\xc5\xa4\xc9\x17"

# Base DN of your directory
app.config["LDAP_USE_SSL"] = True
app.config["LDAP_HOST"] = "ADSRV01.lakesideind.net"
app.config["LDAP_BASE_DN"] = "ou=Lakeside,DC=lakesideind,DC=net"
app.config[
    "LDAP_USERNAME"
] = "CN=svc_bidept,OU=LI-Service Accounts,OU=Lakeside,DC=lakesideind,DC=net"
app.config["LDAP_PASSWORD"] = "b9,L2$wI.xwo0XQV:`kz"
app.config["LDAP_REALM_NAME"] = "Test App"
app.config["LDAP_LOGIN_VIEW"] = "account.login"


ldap.init_app(app)
app.debug = False
toolbar = DebugToolbarExtension(app)


def main():

    app.run(host="0.0.0.0", debug=True
    , use_reloader=False
    )


def register_blueprints():
    from views import home_views
    from views import account_views
    from views import datawarehouse_views
    from views import admin_views
    from views import vista_views

    app.register_blueprint(home_views.blueprint)
    app.register_blueprint(account_views.blueprint)
    app.register_blueprint(datawarehouse_views.blueprint)
    app.register_blueprint(admin_views.blueprint)
    app.register_blueprint(vista_views.blueprint)


def register_hooks(app):
    @app.before_request
    def before_request():
        g.user = None
        if "user_id" in session:
            # This is where you'd query your database to get the user info.
            g.user = "LAKESIDEIND\\" + ldap.get_object_details(user=session["user_id"])[
                "sAMAccountName"
            ][0].decode("utf-8")
            # Create a global with the LDAP groups the user is a member of.
            user_groups = []

            for group in ldap.get_user_groups(user=session["user_id"]):
                user_groups.append(group.decode("utf-8"))

            user_groups.extend(get_app_groups(session["user_id"]))

            g.ldap_groups = user_groups


def register_error_handlers():
    from services import error_handling

    app.register_error_handler(404, error_handling.page_not_found)
    app.register_error_handler(401, error_handling.access_denied)


register_hooks(app)
register_blueprints()
register_error_handlers()

if __name__ == "__main__":
    main()
