from services.connections import automated_tasks


def log(user_name=None, status=None, page=None, full_url=None, unit=None, total=None, message=None):
    connection = automated_tasks()

    cursor = connection.cursor()

    values = [
        user_name,
        status,
        page,
        full_url,
        unit,
        total,
        message
    ]

    cursor.execute('{CALL WebApp.pCreateLog(?, ?, ?, ?, ?, ?, ?)}', values)

    connection.commit()
    connection.close()

    return


def page_requests(user_name, page, full_url, ip_address, referrer):
    connection = automated_tasks()
    cursor = connection.cursor()

    values = [
        page,
        full_url,
        ip_address,
        user_name,
        referrer
    ]

    command = """INSERT INTO WebApp.PageRequests(Page, FullURL, IPAddress, LoggedInUser, Referrer)
                 VALUES (?, ?, ?, ?, ?)"""

    cursor.execute(command, values)

    connection.commit()
    connection.close()


