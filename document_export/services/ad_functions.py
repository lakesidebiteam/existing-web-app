from services.connections import automated_tasks


def get_upn(login):
    connection = automated_tasks()

    cursor = connection.cursor()

    values = [
        login
    ]

    cursor.execute('{CALL WebApp.pUPNLookup(?)}', values)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    if len(data) == 1:
        data = data[0][0]
    else:
        data = login

    return data

def get_app_groups(upn):
    connection = automated_tasks()

    cursor = connection.cursor()

    values = [
        upn
    ]

    cursor.execute('{CALL WebApp.pGetGroupMembership(?)}', values)

    data = cursor.fetchall()

    connection.commit()
    connection.close()

    groups = []

    for row in data:
        groups.append(row[0])

    return groups

