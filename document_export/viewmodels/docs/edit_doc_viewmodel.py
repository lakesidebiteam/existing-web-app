from flask import request, Markup, flash, redirect
from viewmodels.shared.viewmodelbase import ViewModelBase
from lakeside.connection.sql_server import LakesideConnections
import json

class EditDocViewModel(ViewModelBase):
    def __init__(self, category=None, page=None):
        super().__init__()
        self.page_title = "Edit Document"

        self.category = category
        self.page = page

        self.category_id = ''
        self.doc_page_title = ""
        self.doc_page_content = ""
        self.page_id = ''

        self.is_existing = False

        self.lc = LakesideConnections(app="Web App Edit Docs")


    def get_data(self):
        self.get_categories()
        self.get_existing()


    def get_existing(self):
        try:
            con = self.lc.automated_tasks()
            cursor = con.cursor()

            command = """
                SELECT

                    B.PageID
                    ,B.PageName
                    ,B.PageContent
                    ,A.CategoryID

                FROM Docs.Category as A
                LEFT JOIN Docs.Page as B
                    ON A.CategoryID = B.CategoryID AND
                       B.PageURL = ?

                WHERE A.CategoryURL = ?"""

            values = [
                self.page,
                self.category,
            ]

            cursor.execute(command, values)

            data = cursor.fetchone()

            print(data)

            if data:
                self.page_id = data[0]
                self.doc_page_title = data[1]
                self.doc_page_content = Markup(data[2].replace("`", "\`"))
                self.category_id = data[3]
                if self.doc_page_content != "":
                    self.is_existing = True

            print(self.doc_page_content)


        except Exception as e:
            raise e
        finally:
            con.close()



    def get_category_pages(self):
        try:

            data = request.get_json(silent=True)

            con = self.lc.automated_tasks()
            cursor = con.cursor()

            command = """
                SELECT
                
                    B.PageName

                    ,CONCAT(A.CategoryURL, '/', B.PageURL) as URL

                FROM Docs.Category as A
                JOIN Docs.Page as B
                    ON A.CategoryID = B.CategoryID
                
                WHERE A.CategoryID = ?"""

            cursor.execute(command, [data['category_id']])

            data = cursor.fetchall()

        except Exception as e:
            raise e
        finally:
            con.close()

        data_list = []

        for row in data:
            data_list.append(
                {
                    "page_name": row[0],
                    "page_url": row[1],
                }
            )

            print(data_list)


        return Markup(json.dumps(data_list))


    def get_categories(self):
        
        try:
            con = self.lc.automated_tasks()
            cursor = con.cursor()

            command = """
                SELECT

                    A.CategoryID

                    ,A.Category

                    ,COUNT(B.CategoryID) as Count

                FROM Docs.Category as A
                LEFT JOIN Docs.Page as B
                    ON A.CategoryID = B.CategoryID

                GROUP BY A.CategoryID, A.Category

                Order by Category"""

            cursor.execute(command)

            data = cursor.fetchall()

            self.categories = {}

            for row in data:
                self.categories[row[0]] = [row[1], int(row[2])]

        except Exception as e:
            raise e
        finally:
            con.close()

    
    def process_data(self):

        task = self.request_dict.get("task")

        if task == "new-category":
            return self.add_category()

        elif task == 'add-edit':
            return self.save_data()

        elif task == 'get-pages':
            return self.get_category_pages()

        elif task == 'delete-page':
            return self.delete_page()

    def save_data(self):
        try:
            data = request.get_json(silent=True)
            con = self.lc.automated_tasks()
            cursor = con.cursor()
            command = "{ CALL Docs.pAddEditPage(?, ?, ?, ?, ?)}"
            values = [
                None if data['page_id'] == "" else data['page_id'],
                data['category'],
                data['title'],
                data['title'].replace(' ', '-').lower(),
                data['content'],
            ]
            print(values)
            cursor.execute(command, values)
            
            data = cursor.fetchone()

            cursor.commit()
        except Exception as e:
            raise e
        finally:
            con.close()

        return Markup(json.dumps({
                'status': data[0],
                'message': data[1]
            }))

    def add_category(self):

        self.get_categories()
        data = request.get_json(silent=True)
        print(data)

        if data['category'].lower() in [x[0].lower() for x in self.categories.values()]:
            return Markup(json.dumps({
                'status': "error",
                'message': f'{data["category"]} already exists.'
            }))
        else:
            try:
                con = self.lc.automated_tasks()
                cursor = con.cursor()
                command = """
                    INSERT INTO Docs.Category (Category, CategoryURL)
                    OUTPUT Inserted.CategoryID, Inserted.Category
                    VALUES (?, ?)"""

                values = [
                    data['category'],
                    data['category'].replace(' ', '-').lower(),
                ]

                cursor.execute(command, values)

                data = cursor.fetchone()

                con.commit()
            except Exception as e:
                raise e
            finally:
                con.close()

            return Markup(json.dumps({
                'status': 'success',
                'category_id': data[0],
                'category': data[1]
            }))
        
    def delete_page(self):
        data = request.get_json(silent=True)

        page_id = data['page_id']
        page_name = data['page_name']

        try:
            con = self.lc.automated_tasks()
            cursor = con.cursor()

            command = """
                DELETE FROM Docs.Page
                WHERE PageID = ?"""
            
            cursor.execute(command, [page_id])
            cursor.commit()

        except Exception as e:
            raise e
        finally:
            con.close()

        flash(f"{page_name} Successfully Deleted", 'table-success')
        return Markup(json.dumps({'status': "success"}))



            

