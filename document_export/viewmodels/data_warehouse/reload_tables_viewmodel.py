from operator import itemgetter
from viewmodels.shared.viewmodelbase import ViewModelBase
from services.data_warehouse import DataWarehouse
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col, SubmitButtons
from flask import Markup, jsonify
import json
import operator


class ReloadTableViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "Reload Tables"
        self.page_header = "Reload Tables"
        self.message_category = "Reload"
        self.show_filters = (
            False if self.request_dict.get("showfilters", "").upper() == "N" else True
        )
        self.embed = (
            True if self.request_dict.get("embedded", "").upper() == "Y" else False
        )
        self.show_header = (
            False if self.request_dict.get("showheader", "").upper() == "N" else True
        )
        self.submit_buttons = []
        self.action = ""
        self.output = "screen"

        if "ApplyFilters" in self.request_dict.keys():
            self.db_id = self.request_dict.get("Database")
            self.schema_id = self.request_dict.get("Schema")
            self.search = self.request_dict.get("Search", "")
            self.action = "reload"

        else:
            self.db_id = self.request_dict.get("database")
            self.schema_id = self.request_dict.get("schema")
            self.search = self.request_dict.get("search", "")
            self.output = self.request_dict.get("output", "None")

        self.table_dict = {"id": [], "source": [], "sequence": []}

        if "reload_selected" in self.request_dict.keys():
            self.action = "reload_selected"
            for key in self.request_dict.keys():
                if key.startswith("Bulk Reload_"):
                    print(key)
                    id = key.replace("Bulk Reload_", "")
                    self.table_dict["id"].append(id)
                    self.table_dict["source"].append(
                        None
                        if self.request_dict.get(f"Source_{id}", "All") == "All"
                        else self.request_dict.get(f"Source_{id}")
                    )
                    self.table_dict["sequence"].append(
                        None
                        if self.request_dict.get(f"Sequence_{id}", "All") == "All"
                        else self.request_dict.get(f"Sequence_{id}")
                    )

            if len(self.table_dict["id"]) == 0:
                self.action = ""

        self.dw = DataWarehouse()

        self.table_list = self.dw.get_table_list(
            self.db_id, self.schema_id, self.search, "All"
        )

        self.schema_list = self.dw.get_schema_list(default="All")

        self.schema_list = sorted(self.schema_list, key=operator.itemgetter(1))

        self.schema_dict = {"All": {"All": "All"}}

        for schema in self.schema_list:
            if str(schema[2]) != "All":
                if str(schema[2]) not in self.schema_dict.keys():
                    self.schema_dict[str(schema[2])] = {"All": "All"}

                self.schema_dict["All"][str(schema[1])] = str(schema[0])
                self.schema_dict[str(schema[2])][str(schema[1])] = str(schema[0])

        self.schema_dict = Markup(json.dumps(self.schema_dict))

        self.url = gen_url(
            "/dw/reloadtable",
            "",
            database=self.db_id,
            schema=self.schema_id,
            search=self.search if self.search != "" else None,
        )

        self.reload_table_id = None
        self.reload_table_source = None
        self.reload_table_job = False

        for key, value in self.request_dict.items():
            if key.startswith("Reload_"):
                self.reload_table_id = value
                self.reload_table_source = self.request_dict.get(
                    "Source_" + str(value), "All"
                )
                self.action = "reload_table"
            elif key.startswith("Reload Job_"):
                self.reload_table_id = value
                self.reload_table_source = self.request_dict.get(
                    "Source_" + str(value), "All"
                )
                self.reload_table_job = True
                self.action = "reload_table"
            elif key.startswith("Recreate Hash_"):
                self.reload_table_id = value
                self.action = "recreate_hash"

    def create_filters(self):

        filters = [
            create_filter(
                name="Database",
                header="Database",
                filter_type="combo",
                value=self.db_id,
                items=self.dw.get_database_list("All"),
                default_value="All",
            ),
            create_filter(
                name="Schema",
                header="Schema",
                filter_type="combo",
                value=self.schema_id,
                items=[
                    [schema[0], schema[1]]
                    for schema in self.dw.get_schema_list(self.db_id, "All")
                ],
                default_value="All",
            ),
            create_filter(
                name="Search",
                header="Search",
                filter_type="text",
                placeholder="None",
                value=self.search,
                default_value="",
                tooltip="Enter part of the table's full destination name",
            ),
        ]

        return filters

    def build_table(self):

        if len(self.table_list) == 0:
            return {}

        dt = DataTable(get_col(self.table_list, 0))

        dt.create_table_col(
            col_header="Table ID",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.table_list, 0),
        )

        dt.create_table_col(
            col_header="Table Name",
            col_type="",
            col_class="reload-table-table-name text-truncate",
            col_text="",
            col_data=get_col(self.table_list, 1),
        )

        dt.create_table_col(
            col_header="Table Description",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.table_list, 6),
        )

        dt.create_table_col(
            col_header="Database",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.table_list, 2),
        )

        dt.create_table_col(
            col_header="Schema",
            col_type="",
            col_class="",
            col_text="",
            col_data=get_col(self.table_list, 3),
        )

        source_list = []
        for item in get_col(self.table_list, 4):
            item_list = []
            for source in item:
                item_list.append([source, source])
            source_list.append(item_list)

        dt.create_table_col(
            col_header="Source",
            col_type="combo",
            col_class="",
            col_text="",
            col_default="All",
            col_data=source_list,
        )

        dt.create_table_col(
            col_header="Sequence",
            col_type="number",
            col_class="",
            col_text="",
            col_data=get_col(self.table_list, 5),
        )

        dt.create_table_col(
            col_header="Bulk Reload",
            col_type="checkbox",
            col_class="",
            col_text="",
            col_data=get_col(self.table_list, 4),
        )

        dt.create_table_col(
            col_header="Reload",
            col_type="button",
            col_class="",
            col_text="Reload Table",
            col_data=get_col(self.table_list, 0),
        )

        dt.create_table_col(
            col_header="Reload Job",
            col_type="button",
            col_class="",
            col_text="Reload Table (Job)",
            col_data=get_col(self.table_list, 0),
        )

        dt.create_table_col(
            col_header="Recreate Hash",
            col_type="button",
            col_class="",
            col_text="Recreate Hash",
            col_data=get_col(self.table_list, 0),
        )

        return dt.get_table()

    def get_filter_buttons(self):

        sb = SubmitButtons()

        sb.add_button(
            btn_name="reload_selected",
            btn_class="btn-secondary",
            btn_text='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i> Reload Selected',
        )

        return sb.get_buttons()

