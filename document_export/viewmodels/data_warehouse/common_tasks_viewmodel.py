from viewmodels.shared.viewmodelbase import ViewModelBase
from services.data_warehouse import DataWarehouse
from infrastructure.gen_url import gen_url
from services.create_filter import create_filter, DataTable, get_col, SubmitButtons
from flask import Markup


class RunCommonTasks(ViewModelBase):
    def __init__(self):
        super().__init__()

        self.page = "Run Task"
        self.page_header = "Run Task"
        self.message_category = "Task"
       
        self.dw = DataWarehouse()

        self.action_id = None

        self.cards = [{
                    'header': "Deploy Security",
                    'text': "Deploy security settings from DW_Metadata to DW",
                    'id': 1
                },
                {
                    'header': "AD Person",
                    "text": "Reloads SourceData.ActiveDirectory.Person and update PR.dimEmployee with any changes",
                    "id": 2
                }]

                
        
        self.url = '/dw/runpath'

        self.action = ''

        for key, value in self.request_dict.items():
            if key.startswith('submit_'):
                self.action_id = key.replace('submit_', '')
                self.action = 'run_task'
            
