import flask
from flask import Request, g, session
from services.flash_messages import flash_messages
from services.logging import log, page_requests
from services.get_featured_items import get_headers, get_header_items

from infrastructure import request_dict


class ViewModelBase:
    def __init__(self, api=False, container_type='container'):
        self.request: Request = flask.request
        self.request_dict = request_dict.create("")
        self.message_category = "All"
        self.page = None
        self.error = None
        self.user = g.user if hasattr(g, "user") else None
        self.login = session.get("lakeside_login")
        self.groups = g.ldap_groups if hasattr(g, "ldap_groups") else []
        self.request_url = str(self.request.full_path)
        self.email_login = session.get("user_id")
        self.api = api
        self.container_type = container_type

        self.bi_user = "APP_BIUser" in self.groups or "APP_RunPath" in self.groups
        self.bi_full = "APP_BIUser" in self.groups
        self.admin_user = "APP_Admin" in self.groups
        self.beta_user = "APP_Beta" in self.groups
        self.is_manager = "Managers - All Companies" in self.groups
        self.vista_user = "_ERP-Users" in self.groups
        self.it_user = "IT Staff" in self.groups

        self.base_url = self.request.base_url
        self.show_header = True

        self.categories = []

        if self.user:
            self.categories.append("user")
        if self.bi_user or self.admin_user:
            self.categories.append("bi_user")
        if self.bi_full or self.admin_user:
            self.categories.append("bi_full")
        if self.admin_user:
            self.categories.append("admin_user")
        if self.beta_user or self.admin_user:
            self.categories.append("beta_user")
        if self.is_manager or self.admin_user:
            self.categories.append('manager')
        if self.vista_user:
            self.categories.append('vista')
        if self.it_user:
            self.categories.append('it_user')

        if self.user and not self.api:
            self.nav_items = get_headers(self.categories)
            self.nav_item_details = get_header_items(
                self.nav_items.keys(), self.categories
            )
        else:
            self.nav_items = {}
            self.nav_item_details = {}

        

        if not self.api:
            page_requests(
                user_name=self.email_login,
                page=self.request.path,
                full_url=self.request_url,
                ip_address=self.request.remote_addr,
                referrer=self.request.referrer,
            )

    def to_dict(self):
        flash_messages(self.message_category)
        return self.__dict__
